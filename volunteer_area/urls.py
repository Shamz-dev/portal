# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import re_path, path, include
from volunteer_area.views import LandingPage, VolunteerArea
from rest_framework import routers
from .views import PersonInNeedViewSet


router = routers.DefaultRouter()
router.register(r'persons_inneed', PersonInNeedViewSet, basename="personinneed")


urlpatterns = [
    re_path(r'^$', LandingPage.as_view(), name='landing_page'),
    re_path(r'^page', VolunteerArea.as_view(), name='volunteer_page'),
    path('api/v1/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
