from django.apps import AppConfig


class VolunteerAreaConfig(AppConfig):
    name = 'volunteer_area'
