from helperreg.models import User, Tasks
from rest_framework import serializers, permissions


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tasks
        fields = ['title', 'description', 'type']


class PeopleInNeedSerializerLoggedIn(serializers.ModelSerializer):
    tasks_set = TaskSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = ['lat', 'long', 'tasks_set']
        permission_classes = [permissions.IsAuthenticated]
