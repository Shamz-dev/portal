# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.shortcuts import redirect
from django.views.generic import TemplateView
from rest_framework import viewsets

from .serializers import PeopleInNeedSerializerLoggedIn

from helperreg.models import PersonInNeed


class UserIsSmSverified(object):
    user_check_failure_path = 'sms_verify_from'  # can be path, url name or reverse_lazy

    def check_user(self):
        if self.request.user.sms_verified:
            return True

    def user_check_failed(self, request, *args, **kwargs):
        return redirect(self.user_check_failure_path)

    def dispatch(self, request, *args, **kwargs):
        if not self.check_user():
            return self.user_check_failed(request, *args, **kwargs)
        return super(UserIsSmSverified, self).dispatch(request, *args, **kwargs)


class LandingPage(TemplateView):
    template_name = 'landing_page.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


class VolunteerArea(UserIsSmSverified, TemplateView):
    '''
    View of the Volunteerlogged in area (maps etc.)
    '''

    template_name = 'landing_page.html'


class PersonInNeedViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = PersonInNeed.objects.all()
    serializer_class = PeopleInNeedSerializerLoggedIn
    # permission_classes = [permissions.IsAuthenticated]
