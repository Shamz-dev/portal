from django.db.models.signals import pre_save
from django.dispatch import receiver
from opencage.geocoder import OpenCageGeocode
from helperreg.models import PersonInNeed, User
from django.conf import settings


@receiver(pre_save, sender=PersonInNeed)
def model_Person_in_need_pre_save(sender, **kwargs):
    ###
    # receive geo data before saving the instance
    ###
    if not settings.DEBUG:
        geocoder = OpenCageGeocode(settings.CAGE_GEOCODE_TOKEN)
        query = kwargs['instance'].street_name+" " +\
            kwargs['instance'].street_number + \
            str(kwargs['instance'].postal_code)
        results = geocoder.geocode(query)
        kwargs['instance'].lat = results[0]['geometry']['lat']
        kwargs['instance'].long = results[0]['geometry']['lng']


@receiver(pre_save, sender=User)
def model_user_pre_save(sender, **kwargs):
    ###
    # receive geo data before saving the instance
    ###
    if not settings.DEBUG:
        geocoder = OpenCageGeocode(settings.CAGE_GEOCODE_TOKEN)
        query = kwargs['instance'].street_name+" " + \
            kwargs['instance'].street_number + \
            str(kwargs['instance'].postal_code)
        results = geocoder.geocode(query)
        kwargs['instance'].lat = results[0]['geometry']['lat']
        kwargs['instance'].long = results[0]['geometry']['lng']
