# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Create your models here.
# -*- coding: utf-8 -*-
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.core.mail import send_mail
from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import ugettext_lazy as _

CATEGORIES = (("shopping", _("Einkaufen gehen")),
              ("other", _("Andere Erledigungen")),
              ("fon_care", _("Betreuung am Telefon")),
              ("remote_work", _("Hilfe bei der Nahrungsmittelbeschaffung online"))
              )

LANGUAGES = (("de", _("Deutsch")),
             ("en", _("Englisch")),
             ("arb", _("Arabisch")),
             ("es", _("Spanisch")),
             ("rus", _("Russisch"))
             )


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')

        languages = []
        topics = []
        if extra_fields.get("languages"):
            for lang in extra_fields.get("languages"):
                obj = Languages.objects.get_or_create(name=lang)
                languages.append(obj)
            del extra_fields['languages']
        if extra_fields.get("help_topics"):
            for topic in extra_fields.get("help_topics"):
                obj = HelpCategories.objects.get_or_create(name=topic)
                topics.append(obj)
            del extra_fields['help_topics']
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        for lang in languages:
            user.languages.add(lang[0])
        for topic in topics:
            user.help_topics.add(topic[0])
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        # all data ist saved into email that's why we need to copy the data
        # into extrafields
        extra_fields = dict(email)
        email = extra_fields['email']
        try:
            del extra_fields['email']
            del extra_fields['password1']
            del extra_fields['password2']
        except KeyError:
            pass

        extra_fields.setdefault('is_superuser', False)
        extra_fields.setdefault('is_staff', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_staff', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User (AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), unique=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    is_active = models.BooleanField(_('active'), default=True)
    sms_verifyer_token = models.CharField(null=True, max_length=10)
    is_staff = models.BooleanField(default=False)
    is_hotline_volunteer = models.BooleanField(default=False)
    updated_at = models.DateTimeField(auto_now=True)
    fon = models.CharField(max_length=100, verbose_name=_("Telefonnummer"),
                           help_text=_("Bitte im Format +49(erste Null weglassen) 152337182 etc."))
    postal_code = models.IntegerField(verbose_name=_("PLZ"), null=True)
    city = models.CharField(max_length=255, verbose_name=_("Ort"))
    street_name = models.CharField(max_length=255, verbose_name=_("Straßenname"))
    street_number = models.CharField(max_length=10, verbose_name=_("Hausnummer"))
    sms_verified = models.BooleanField(default=False)
    languages = models.ManyToManyField("Languages", verbose_name=_("Welche Sprachen sprichst Du?"))
    helped_persons = models.ManyToManyField("PersonInNeed",
                                            verbose_name=_("Hilfesuchende"),
                                            related_name="person_in_need", blank=True)

    blocked = models.BooleanField(default=False)
    number_of_activation_sms_sent = models.SmallIntegerField(default=1)
    lat = models.CharField(max_length=100, verbose_name="latitude", null=True, blank=True)
    long = models.CharField(max_length=100, verbose_name="longitude", null=True, blank=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)


class Languages(models.Model):
    name = models.CharField(choices=LANGUAGES, max_length=10)

    def __str__(self):
        return self.name


class HelpCategories(models.Model):
    name = models.CharField(choices=CATEGORIES, max_length=20)

    def __str__(self):
        return self.name


class PersonInNeed(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    first_name = models.CharField(max_length=100, verbose_name="Vorname")
    last_name = models.CharField(max_length=255, verbose_name="Nachname")
    fon = models.CharField(max_length=100, verbose_name="Telefonnummer",
                           help_text="Bitte im Format +49(erste Null weglassen) 152337182 etc.")
    postal_code = models.IntegerField(verbose_name="PLZ")
    street_name = models.CharField(max_length=255, verbose_name="Straßenname")
    street_number = models.CharField(max_length=10, verbose_name="Hausnummer")
    email_address = models.EmailField(null=True, blank=True)
    language = models.ManyToManyField(Languages)
    lat = models.CharField(max_length=100, verbose_name="latitude", null=True, blank=True)
    long = models.CharField(max_length=100, verbose_name="longitude", null=True, blank=True)

    def __str__(self):
        return self.first_name + self.last_name


class Tasks(models.Model):
    class TaskChoices(models.TextChoices):
        ONCE = 'once', _('einmalig')
        RECURRING = 'RC', _('regelmäßig')

    created = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=255, verbose_name=_("Aufgabe"))
    description = models.TextField(blank=True)
    type = models.CharField(max_length=10, choices=TaskChoices.choices,
                            default=TaskChoices.ONCE, verbose_name=_("aufgabenart"))
    done = models.BooleanField(default=False, verbose_name="erledigt")
    helper = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    person_in_need = models.ForeignKey(PersonInNeed, null=True, blank=True, on_delete=models.CASCADE)
    help_topics = models.ForeignKey("HelpCategories", on_delete=models.CASCADE, verbose_name=_("Hilfethema"), null=True)

    def __str__(self):
        return self.title


default_app_config = 'helperreg.apps.HelperregConfig'
