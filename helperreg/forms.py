# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.forms import ModelForm
from django_registration.forms import RegistrationForm, forms
from .models import User
from .models import LANGUAGES, CATEGORIES


class HelperForm(RegistrationForm):
    languages = forms.MultipleChoiceField(choices=LANGUAGES, widget=forms.CheckboxSelectMultiple())
    help_topics = forms.MultipleChoiceField(choices=CATEGORIES, widget=forms.CheckboxSelectMultiple())

    class Meta(RegistrationForm.Meta):
        model = User
        fields = ['first_name',
                  'last_name',
                  'email',
                  'fon',
                  'street_name',
                  'street_number',
                  'postal_code',
                  'city',
                  'help_topics',
                  'languages']


class SmSverifyForm(ModelForm):
    class Meta(RegistrationForm.Meta):
        model = User
        fields = ['sms_verifyer_token']
