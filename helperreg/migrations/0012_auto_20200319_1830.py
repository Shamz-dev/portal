# Generated by Django 3.0.4 on 2020-03-19 17:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('helperreg', '0011_auto_20200319_1806'),
    ]

    operations = [
        migrations.AlterField(
            model_name='personinneed',
            name='email_address',
            field=models.EmailField(blank=True, max_length=254, null=True),
        ),
    ]
