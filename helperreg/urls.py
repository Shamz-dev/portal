# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import url
from django.urls import include, path
from django.views.generic import TemplateView
from .forms import HelperForm

from .views import HelperCreateView, SmSverifyerView

urlpatterns = [

        url(r'^accounts/register/$',
            HelperCreateView.as_view(form_class=HelperForm),
            name='registration_register'),

        path(r'accounts/', include('django_registration.backends.activation.urls')),
        path('accounts/', include('django.contrib.auth.urls')),
        path(r'thanks/', TemplateView.as_view(template_name="thankyou.html"), name='thankyou'),
        path(r'smsverify/', SmSverifyerView.as_view(), name='sms_verify_from'),
]
